﻿# Projet INFRA - Guillet Florian / Barreaux Thomas
## Guide d'installation des différents outils

 1. Samba
 - Installation
 ```
 Sudo dnf install samba
 ```
- Lancer samba
 ```
 Sudo systemctl start samba
 ```
 - Configurer l'accès à Samba
```
vim /etc/samba/smb.conf


[MyShare]
       path = /home/spak/share-folder
       comment = samba
       browseable = yes
       writable = yes
       guest ok = yes
       read only = no
```
- Crée le dossier de partage
```
mkdir /home/spak/share-folder
```
- Donner les permissions au dossier
```
chmod 777 /home/spak/share-folder
```
- Crée un fichier dans le dossier de partage
```
touch /home/spak/share-folder
```
- Crée un utilisateur afin de bien répartir les droits sur les différents utilisateurs de la machine
```
useradd sambauser
```
- Crée un mot de passe à l'utilisateur samba
```
smbpasswd -a sambauser
Retype new SMB password:
Added user sambauser.
```
- Redémarrer le service samba
```
Sudo systemctl restart smb
```
- Vérifier que samba est bien lancé
```
systemctl status smb
```
- Pour y accéder depuis la 2ème machine
```
smbclient //IPMACHINE1/MyShare -U root
```
- Commandes
```
get nomfichier = prendre un fichier de la machine 1 vers la machine 2

put nomfichier = mettre un fichier de la machine 2 vers la machine 1
```

 2. Node exporter
 - Installer wget
 ```
 Sudo dnf install wget
 ```
 - Télécharger l'archive node
```
wget https://github.com/prometheus/node_exporter/releases/download/v1.0.1/node_exporter-1.0.1.linux-amd64.tar.gz
```
- Extraire l'archive
```
`dpkg -xvf node_exporter-1.0.1.linux-amd64.tar.gz`
```
- Déplacer l'archive dans un  répertoire qui permet d'être gérer par le système
```
mv exporter-1.0.1.linux-amd64/node_exporter /usr/local/bin/
```
- Création de l'utilisateur pour node exporter
```
useradd -rs /bin/false node_exporter
```
- Installation de nano
```
sudo dnf install nano
```
- Création du service node exporter
```
nano /etc/systemd/system/node_exporter.service
```
- Le fichier doit contenir les informations suivantes
```
[Unit]
Description=Node Exporter
After=network.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
```
- Recharger le daemon
```
systemctl daemon-reload
```
- Démarrer node exporter
```
systemctl start node_exporter
```
- Vérifier que node exporte est bien lancé
```
systemctl status node_exporter
```
- Pour savoir si tout fonctionne 
```
curl [http://localhost:9100/metrics](http://localhost:9100/metrics)
```
 3. Prometheus
 - Télécharger l'archive dans /usr/src
 ```
 cd /usr/src  
wget https://github.com/prometheus/prometheus/releases/download/v2.31.1/prometheus-2.31.1.linux-amd64.tar.gz
```
- Extraire l'archive
```
tar -xf prometheus-2.31.1.linux-amd64.tar.gz
```
-  Coller tout le dossier dans /etc/prometheus
``` 
cp prometheus-2.31.1.linux-amd64 /etc/prometheus
```
- Ajout de l'host
```
vim /etc/prometheus/prometheus.yml
```
- Ajouter un target avec l'ip de la machine 
```
  - job_name: 'node_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9100']
      - targets: ['192.168.0.2:9100']
```
- Redémarrer prometheus
```
Sudo systemctl restart prometheus
```
 - Vérifier que prometheus tourne bien
 ```
 Sudo systemctl status prometheus
 ```
 4. Grafana
 - Installer grafana 
 ```
 Sudo dnf install grafana
```
- Ajouter le port 3000 au firewall
```
firewall-cmd –add-port=3000/tcp –permanent
firewall-cmd –reload 
```
- Lancer Grafana
```
Sudo systemctl start grafana-server
```
- Vérifier que Grafana est bien start
```
Sudo systemctl status Grafana
```
- Ajouter le dashboard node exporter a grafana
```
Aller dans dashboard  -> Import -> Dans la partie ID coller ceci 1860
```
 5. Clamav
- Installer Clamav
```
Sudo dnf install clamav clamd clamav-update
```
- Lancer la mise a jour  de la base de donnée clamav afin d'avoir les dernières signatures virus en date
```
sudo freshclam
 ```
 - Pour faire un scan basique
 ```
clamscan /path/to/directory
```
- Pour faire un scan récursif
 ```
 clamscan -r /path/to/directory
 ```
6. Lynis
- Installation
```
Sudo dnf install lynis
```
- Pour faire un audit du système
```
lynis audit system
```
 7. Cracklib
 - Installer cracklib
 ```
 sudo dnf install libpam-cracklib
 ```
 - Vérifier la force d'un mot de passe
 ```
 echo 'lemotdepasse' | cracklib-check
 ```
 

